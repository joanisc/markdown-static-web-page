#!/bin/bash
pandoc content.md -f markdown -t html -s -o index.html
sed -i '/<!DOCTYPE/,/<body>/d' index.html
cp index.txt temporal_index.txt 
cat index.html >> temporal_index.txt 
rm index.html
mv temporal_index.txt index.html
sed -i 's/<table/<table class="sortable"/g' index.html
