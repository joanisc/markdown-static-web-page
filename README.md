# README

### Dependencies
- GNU/Linux like OS
- Pandoc

### How to use it

1) Install pandoc => `https://pandoc.org/installing.html`
2) Write your top menu index on the file `index.txt` in html (Sorry)
3) Write your text content on the file `content.md`

Are you ready to convert into a webpage? 

4) Execute the script that will merge the index and the content to generate a new file index.html 
`/scriptHead.sh content.md`
5) You can upload your new webpage index.html to any web server.


### Optional
You can define your style in css by editing style.css


### Other
> To improve the experience this repo includes 2 scripts to sort tables and to jump to top.
scriptTop.js
sorttable.js

* Sortable has their own license and repo https://github.com/SortableJS/Sortable

---

You can see a demo of this repo working here: http://singlacasamitjana.eu/milibera/index.html
